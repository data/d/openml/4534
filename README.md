# OpenML dataset: PhishingWebsites

https://www.openml.org/d/4534

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Rami Mustafa A Mohammad ( University of Huddersfield","rami.mohammad '@' hud.ac.uk","rami.mustafa.a '@' gmail.com) Lee McCluskey (University of Huddersfield","t.l.mccluskey '@' hud.ac.uk )  Fadi Thabtah (Canadian University of Dubai","fadi '@' cud.ac.ae)  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/phishing+websites)  
**Please cite**: Please refer to the [Machine Learning Repository's citation policy](https://archive.ics.uci.edu/ml/citation_policy.html)  

Source:

Rami Mustafa A Mohammad ( University of Huddersfield, rami.mohammad '@' hud.ac.uk, rami.mustafa.a '@' gmail.com)
Lee McCluskey (University of Huddersfield,t.l.mccluskey '@' hud.ac.uk )
Fadi Thabtah (Canadian University of Dubai,fadi '@' cud.ac.ae)


Data Set Information:

One of the challenges faced by our research was the unavailability of reliable training datasets. In fact this challenge faces any researcher in the field. However, although plenty of articles about predicting phishing websites have been disseminated these days, no reliable training dataset has been published publically, may be because there is no agreement in literature on the definitive features that characterize phishing webpages, hence it is difficult to shape a dataset that covers all possible features. 
In this dataset, we shed light on the important features that have proved to be sound and effective in predicting phishing websites. In addition, we propose some new features.


Attribute Information:

For Further information about the features see the features file in the [data folder](https://archive.ics.uci.edu/ml/machine-learning-databases/00327/Phishing Websites Features.docx) of UCI.

Relevant Papers:

Mohammad, Rami, McCluskey, T.L. and Thabtah, Fadi (2012) An Assessment of Features Related to Phishing Websites using an Automated Technique. In: International Conferece For Internet Technology And Secured Transactions. ICITST 2012 . IEEE, London, UK, pp. 492-497. ISBN 978-1-4673-5325-0

Mohammad, Rami, Thabtah, Fadi Abdeljaber and McCluskey, T.L. (2014) Predicting phishing websites based on self-structuring neural network. Neural Computing and Applications, 25 (2). pp. 443-458. ISSN 0941-0643

Mohammad, Rami, McCluskey, T.L. and Thabtah, Fadi Abdeljaber (2014) Intelligent Rule based Phishing Websites Classification. IET Information Security, 8 (3). pp. 153-160. ISSN 1751-8709

 

Citation Request:

Please refer to the Machine Learning Repository's citation policy

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4534) of an [OpenML dataset](https://www.openml.org/d/4534). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4534/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4534/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4534/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

